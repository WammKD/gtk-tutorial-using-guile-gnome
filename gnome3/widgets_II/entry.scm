(use-modules (gi) (gi repository) (gi types))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "AttachOptions")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Entry")
(load-by-name "Gtk" "Label")
(load-by-name "Gtk" "Table")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.entry"))

(define FILLorSHRINK (list->flags <GtkAttachOptions> '(fill shrink)))

(define (entry app)
  (let ([window (make <GtkWindow> #:application     app
                                  #:border-width    10
                                  #:window-position (symbol->window-position
                                                      'center)
                                  #:title           "GtkEntry")]
        [table  (make <GtkTable>  #:n-rows 3 #:n-columns 2 #:homogeneous #f)]
        [label1 (make <GtkLabel>  #:label "Name")]
        [label2 (make <GtkLabel>  #:label "Age")]
        [label3 (make <GtkLabel>  #:label "Occupation")]
        [entry1 (make <GtkEntry>)]
        [entry2 (make <GtkEntry>)]
        [entry3 (make <GtkEntry>)])
    (add window table)

    (attach table label1 0 1 0 1 FILLorSHRINK FILLorSHRINK 5 5)
    (attach table label2 0 1 1 2 FILLorSHRINK FILLorSHRINK 5 5)
    (attach table label3 0 1 2 3 FILLorSHRINK FILLorSHRINK 5 5)
    (attach table entry1 1 2 0 1 FILLorSHRINK FILLorSHRINK 5 5)
    (attach table entry2 1 2 1 2 FILLorSHRINK FILLorSHRINK 5 5)
    (attach table entry3 1 2 2 3 FILLorSHRINK FILLorSHRINK 5 5)

    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate entry)
(run app (command-line))
