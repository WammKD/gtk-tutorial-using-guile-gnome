(use-modules (gi) (gi repository) (gi types))

(require "Gdk" "3.0")
(require "Gio" "2.0")
(require "Gtk" "3.0")

;; (load-by-name "Gdk" "Pixbuf") ;; add GdkPixbuf to the list of missing stuff
(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Widget") ;; show-all
