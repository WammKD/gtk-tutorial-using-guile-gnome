(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "ComboBox") ;; changed
(load-by-name "Gtk" "ComboBoxText")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "HBox")
(load-by-name "Gtk" "Label")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.comboLabel"))

(define (combo-selected comboBox lbl)
  (set-text lbl (get-active-text comboBox)))

(define (combo app)
  (let ([window (make <GtkWindow>       #:application     app
                                        #:default-width   300
                                        #:default-height  200
                                        #:border-width    15
                                        #:window-position (symbol->window-position
                                                            'center)
                                        #:title           "GtkComboBox")]
        [hbox   (make <GtkHBox>         #:homogeneous #f #:spacing 0)]
        [vbox   (make <GtkVBox>         #:homogeneous #f #:spacing 15)]
        [combo  (make <GtkComboBoxText>)]
        [label  (make <GtkLabel>        #:label "…")])
    (append-text combo "Ubuntu")
    (append-text combo "Arch")
    (append-text combo "Fedora")
    (append-text combo "Mint")
    (append-text combo "Gentoo")
    (append-text combo "Debian")

    (pack-start vbox combo #f #f 0)
    (pack-start vbox label #f #f 0)
    (pack-start hbox vbox  #f #f 0)

    (add window hbox)

    (connect window destroy (lambda (w) (quit)))
    (connect combo  changed (lambda (c) (combo-selected c label)))

    (show-all window)))

(connect app activate combo)
(run app (command-line))
