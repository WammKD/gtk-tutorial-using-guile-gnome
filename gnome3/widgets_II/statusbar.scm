(use-modules (gi) (gi repository) (gi types))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Alignment")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Button")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "HBox")
(load-by-name "Gtk" "Statusbar")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.statusbar"))

(define (button-pressed button stsBar)
  (let ([str (string-append (get-label button) " button clicked")])
    (push stsBar (get-context-id stsBar str) str)))

(define (statusbar app)
  (let ([window    (make <GtkWindow>    #:application     app
                                        #:default-width   300
                                        #:default-height  200
                                        #:window-position (symbol->window-position
                                                            'center)
                                        #:title           "GtkStatusbar")]
        [hbox      (make <GtkHBox>      #:homogeneous #f #:spacing 0)]
        [vbox      (make <GtkVBox>      #:homogeneous #f #:spacing 0)]
        [halign    (make <GtkAlignment> #:xalign 0 #:yalign 0
                                        #:xscale 0 #:yscale 0)]
        [balign    (make <GtkAlignment> #:xalign 0 #:yalign 1
                                        #:xscale 1 #:yscale 0)]
        [button1   (make <GtkButton>    #:label          "OK"
                                        #:width-request  70
                                        #:height-request 30)]
        [button2   (make <GtkButton>    #:label          "Apply"
                                        #:width-request  70
                                        #:height-request 30)]
        [statusbar (make <GtkStatusbar>)])
    (add window vbox)
    (add halign hbox)

    (pack-start vbox halign  #t #t 5)
    (pack-start hbox button1 #f #f 5)
    (pack-start hbox button2 #f #f 0)

    (add balign statusbar)

    (pack-start vbox balign #f #f 0)

    (connect button1 clicked (lambda (b1) (button-pressed b1 statusbar)))
    (connect button2 clicked (lambda (b2) (button-pressed b2 statusbar)))
    (connect window  destroy (lambda (w)  (quit)))

    (show-all window)))

(connect app activate statusbar)
(run app (command-line))
