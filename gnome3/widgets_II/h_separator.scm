(use-modules (gi) (gi repository) (gi types))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "HSeparator")
(load-by-name "Gtk" "Label")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.entry"))

(define (separator app)
  (let ([window     (make <GtkWindow>     #:application     app
                                          #:border-width    10
                                          #:resizable       #f
                                          #:window-position (symbol->window-position
                                                              'center)
                                          #:title           "GtkHSeparator")]
        [label1     (make <GtkLabel>      #:label     (string-append
                                                        "Zinc is a moderately reactive, blue gray "
                                                        "metal that tarnishes in moist air and "
                                                        "burns in air with a bright bluish-green "
                                                        "flame, giving off fumes of zinc oxide. "
                                                        "It reacts with acids, alkalis and other "
                                                        "non-metals. If not completely pure, zinc "
                                                        "reacts with dilute acids to release "
                                                        "hydrogen.")
                                          #:wrap #t)]
        [label2     (make <GtkLabel>      #:label     (string-append
                                                        "Copper is an essential trace nutrient to "
                                                        "all high plants and animals. In animals, "
                                                        "including humans, it is found primarily "
                                                        "in the bloodstream, as a co-factor in "
                                                        "various enzymes, and in copper-based "
                                                        "pigments. However, in sufficient "
                                                        "amounts, copper can be poisonous and "
                                                        "even fatal to organisms.")
                                          #:wrap #t)]
        [hseparator (make <GtkHSeparator>)]
        [vbox       (make <GtkVBox>       #:homogeneous #f #:spacing 10)])
    (add window vbox)

    (pack-start vbox label1     #f #t  0)
    (pack-start vbox hseparator #f #t 10)
    (pack-start vbox label2     #f #t  0)

    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate separator)
(run app (command-line))
