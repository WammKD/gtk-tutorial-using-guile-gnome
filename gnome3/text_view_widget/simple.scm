(use-modules (gi) (gi repository))

;; Somehow, one of these loads "Pango" "1.0"
(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio"   "Application") ;; activate, run
(load-by-name "Gtk"   "Application")
(load-by-name "Gtk"   "ApplicationWindow")
(load-by-name "Gtk"   "WindowPosition")
(load-by-name "Gtk"   "Box") ;; pack-start
(load-by-name "Gtk"   "Container") ;; add
(load-by-name "Gtk"   "TextBuffer")
(load-by-name "Gtk"   "TextIter")
(load-by-name "Gtk"   "TextTag")
(load-by-name "Gtk"   "TextTagTable")
(load-by-name "Gtk"   "TextView")
(load-by-name "Gtk"   "VBox")
(load-by-name "Gtk"   "Widget") ;; show-all
(load-by-name "Gtk"   "WrapMode")
(load-by-name "Pango" "Style")  ;; (symbol->style  number->style   style->symbol  style->number <PangoStyle>)
(load-by-name "Pango" "Weight") ;; (symbol->weight number->weight weight->symbol weight->number <PangoWeight>)

(define app (make <GtkApplication> #:application-id "org.gtk.simple"))

(define (simple app)
  (let* ([window    (make <GtkWindow>     #:application     app
                                          #:default-width   300
                                          #:default-height  200
                                          #:window-position (symbol->window-position
                                                              'center)
                                          #:title           "GtkTextView")]
         [view      (make <GtkTextView>   #:wrap-mode       (symbol->wrap-mode
                                                              'word))]
         [vbox      (make <GtkVBox>       #:homogeneous     #f
                                          #:spacing         0)]
         [buffer   (get-buffer view)])
    (pack-start vbox view #t #t 0)

    (let ([tagTable (get-tag-table buffer)])
      ;;; All of this was what we did for Gnome2 but, even though
      ;;; `gtk_text_buffer_create_tag` exists in the Gnome3 documentation
      ;;; (https://developer.gnome.org/gtk3/stable/GtkTextBuffer.html#gtk-text-buffer-create-tag),
      ;;; I can't seem to find a `text-buffer:create-tag` method anywhere in
      ;;; Guile-GI
      ;; (create-tag buffer "gap"     'pixels-above-lines                 30)
      (add? tagTable (make <GtkTextTag> #:name               "gap"
                                        #:pixels-above-lines 30))
      ;; (create-tag buffer "lmarg"   'left-margin                         5)
      (add? tagTable (make <GtkTextTag> #:name        "lmarg"
                                        #:left-margin 5))
      ;; (create-tag buffer "blue_fg" 'foreground                     "blue")
      (add? tagTable (make <GtkTextTag> #:name       "blue_fg"
                                        #:foreground "blue"))
      ;; (create-tag buffer "gray_bg" 'background                     "gray")
      (add? tagTable (make <GtkTextTag> #:name       "gray_bg"
                                        #:foreground "gray"))
      ;; (create-tag buffer "italic"  'style              (genum->value
      ;;                                                    (make <pango-style>
      ;;                                                      #:value 'italic)))
      (add? tagTable (make <GtkTextTag> #:name  "italic"
                                        #:style (symbol->style 'italic)))
      ;; (create-tag buffer "bold"    'weight             (genum->value
      ;;                                                    (make <pango-weight>
      ;;                                                      #:value   'bold)))
      (add? tagTable (make <GtkTextTag> #:name   "bold"
                                        #:weight (weight->number
                                                   (symbol->weight 'bold)))))

    (let ([iter     (get-iter-at-offset! buffer (make <GtkTextIter>) 0)])
      (insert buffer iter "Plain text\n" -1)
      ;;; Likewise, this was the code for Gnome2 but Guile-GI doesn't seem to
      ;;; have the method `text-buffer:insert-with-tags-by-name` while it
      ;;; seems to exist in Gnome3:
      ;;; https://developer.gnome.org/gtk3/stable/GtkTextBuffer.html#gtk-text-buffer-insert-with-tags-by-name
      ;;;
      ;;; The below works but…`insert-with-tags-by-name` would've been a lot easier…
      ;; (insert-with-tags-by-name buffer iter "Colored Text\n"                 '("bold"   "lmarg"))
      (insert buffer iter "Colored text\n" -1)
      (let ([iterAt11 (get-iter-at-offset! buffer (make <GtkTextIter>) 11)]
            [iterAt34 (get-iter-at-offset! buffer (make <GtkTextIter>) 34)])
        (apply-tag-by-name buffer "lmarg"   iterAt11 iterAt34)
        (apply-tag-by-name buffer "blue_fg" iterAt11 iterAt34))
      ;; (insert-with-tags-by-name buffer iter "Text with colored background\n" '("lmarg"  "gray_bg"))
      (insert buffer iter "Text with colored background\n" -1)
      (let ([iterAt24 (get-iter-at-offset! buffer (make <GtkTextIter>) 24)]
            [iterAt52 (get-iter-at-offset! buffer (make <GtkTextIter>) 52)])
        (apply-tag-by-name buffer "lmarg"   iterAt24 iterAt52)
        (apply-tag-by-name buffer "gray_bg" iterAt24 iterAt52))
      ;; (insert-with-tags-by-name buffer iter "Text in italics\n"              '("italic" "lmarg"))
      (insert buffer iter "Text in italics\n" -1)
      (let ([iterAt53 (get-iter-at-offset! buffer (make <GtkTextIter>) 53)]
            [iterAt68 (get-iter-at-offset! buffer (make <GtkTextIter>) 68)])
        (apply-tag-by-name buffer "lmarg"  iterAt53 iterAt68)
        (apply-tag-by-name buffer "italic" iterAt53 iterAt68))
      ;; (insert-with-tags-by-name buffer iter "Bold text\n"                    '("bold"   "lmarg"))
      (insert buffer iter "Bold text\n" -1)
      (let ([iterAt69 (get-iter-at-offset! buffer (make <GtkTextIter>) 69)]
            [iterAt78 (get-iter-at-offset! buffer (make <GtkTextIter>) 78)])
        (apply-tag-by-name buffer "lmarg" iterAt69 iterAt78)
        (apply-tag-by-name buffer "bold"  iterAt69 iterAt78)))

    (add window vbox)

    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate simple)
(run app (command-line))
