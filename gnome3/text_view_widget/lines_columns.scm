(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Statusbar")
(load-by-name "Gtk" "TextBuffer")
(load-by-name "Gtk" "TextIter")
(load-by-name "Gtk" "TextView")
(load-by-name "Gtk" "Toolbar")
(load-by-name "Gtk" "ToolbarStyle")
(load-by-name "Gtk" "ToolButton")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all
(load-by-name "Gtk" "WrapMode")

(define app (make <GtkApplication> #:application-id "org.gtk.linesColumns"))

(define (update-statusbar bffr sttsBar)
  (define iter (get-iter-at-mark! bffr (make <GtkTextIter>) (get-insert bffr)))
  (define row  (get-line        iter))
  (define col  (get-line-offset iter))
  (define msg  (string-append "Col: " (number->string (1+ col))
                              " Ln: " (number->string (1+ row))))

  (pop  sttsBar 0)
  (push sttsBar 0 msg))

(define (mark-set-callback buff statusB)
  (update-statusbar buff statusB))  ; in the original code, they had to
                                    ; handle two more args being passed

(define (lines-columns app)
  (let* ([window    (make <GtkWindow>     #:application     app
                                          #:default-width   230
                                          #:default-height  150
                                          #:window-position (symbol->window-position
                                                              'center)
                                          #:title           "Lines & columns")]
         [vbox      (make <GtkVBox>       #:homogeneous     #f
                                          #:spacing         0)]
         [toolbar   (make <GtkToolbar>    #:toolbar-style   (symbol->toolbar-style
                                                              'icons))]
         [view      (make <GtkTextView>   #:wrap-mode       (symbol->wrap-mode
                                                              'word))]
         [statusbar (make <GtkStatusbar>)]
         [exit      (make <GtkToolButton> #:stock-id        "gtk-quit")]
         [buffer    (get-buffer view)])
    (add window vbox)

    (insert toolbar exit -1)

    (pack-start vbox toolbar   #f #f 5)
    (pack-start vbox view      #t #t 0)

    (grab-focus view)

    (pack-start vbox statusbar #f #f 0)

    (connect exit   clicked  (lambda (e) (quit)))
    (connect buffer changed  (lambda (b) (update-statusbar b statusbar)))
    (connect buffer mark-set (lambda (buff newLocation mark)
                               (mark-set-callback buff statusbar)))
    (connect window destroy  (lambda (w) (quit)))

    (show-all window)

    (update-statusbar buffer statusbar)))

(connect app activate lines-columns)
(run app (command-line))
