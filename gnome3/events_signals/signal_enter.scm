(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gdk" "3.0")
(require "Gtk" "3.0")

(load-by-name "Gdk" "Color")
(load-by-name "Gdk" "Display")
(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Alignment")
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Button")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "StateFlags")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.signalEnter"))

(define (enter-button button)
  (display "Ya hovered!")
  (newline)

  ; widget:override-background-color doesn't seem to work but this is the idea
  (override-background-color
    button
    (list->state-flags '(prelight))
    (make <GdkColor> #:pixel 0 #:red 27000 #:green 30000 #:blue 35000)))

(define (enter a)
  (let ([window   (make <GtkWindow>      #:application     a
                                         #:border-width    15
                                         #:default-width   300
                                         #:default-height  200
                                         #:window-position (symbol->window-position
                                                             'center)
                                         #:title           "Enter signal")]
        [halign   (make <GtkAlignment>   #:xalign 0 #:yalign 0
                                         #:xscale 0 #:yscale 0)]
        [btn      (make <GtkButton>      #:label          "Button"
                                         #:width-request  70
                                         #:height-request 30)])
    (add halign btn)
    (add window halign)

    (connect btn button:enter enter-button)

    (show-all window)))

(connect app activate enter)
(run app (command-line))
