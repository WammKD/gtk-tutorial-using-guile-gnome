(use-modules (gi) (gi repository) (gi types) (srfi srfi-11))

(require "Gio" "2.0")
(require "Gdk" "3.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gdk" "Event") ;; get-event-type
(load-by-name "Gdk" "EventMask")
(load-by-name "Gdk" "EventType")
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "Window") ;; set-title
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Widget") ;; show-all, add-events

(define app (make <GtkApplication> #:application-id "org.gtk.cornerButtons"))

(define (configure-callback windo event)
  (when (equal? (get-event-type event) (symbol->enum <GdkEventType> 'configure))
    (let-values ([(ret x y) (get-coords event)])
      (set-title windo (string-append (number->string x) ", " (number->string y)))))

  #t)

(define (move a)
  (let ([window (make <GtkWindow> #:application     a
                                  #:border-width    15
                                  #:default-width   300
                                  #:default-height  200
                                  #:window-position (symbol->enum
                                                      <GtkWindowPosition>
                                                      'center))])
    ;; The example clearly shows feeding "GDK_CONFIGURE", a <GdkEventType>, to
    ;; gtk_widget_add_events but Guile shows gtk-widget-add-events/add-events as
    ;; taking a <GdkEventMask>.
    ;;
    ;; However, – like the Gnome2 library – ignoring this portion of the code,
    ;; entirely, doesn't seem to affect anything as connect clearly is able to
    ;; pick up configure events; the tutorial (and the GTK3 documentation)
    ;; mentions that the event mask of the widget will determine what kind of
    ;; events it will receive and, yet, configure events are clearly still
    ;; showing up.
    ;;
    ;; The configure-callback function doesn't seem to be implimented so we
    ;; can't filter for only those but we can do it manually, still…
    ;; (add-events window (flags->number
    ;;                      (symbol->enum <GdkEventMask> 'structure-mask)))

    (connect window event configure-callback)

    (show-all window)))

(connect app activate move)
(run app (command-line))
