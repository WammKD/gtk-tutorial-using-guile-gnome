(use-modules (gi) (gi repository) (cairo) (gi compat) (srfi  srfi-19))

(require "Gio"  "2.0")
(require "GLib" "2.0")
(require "Gtk"  "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow") ;; <GtkWindow>
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "DrawingArea")
(load-by-name "Gtk" "Widget") ;; show-all
;; I can't figure out the name for these info.s, yet, so we have to do this
(load (list-ref (infos "GLib") 189)) ;; PRIORITY_DEFAULT
(load (list-ref (infos "GLib") 709)) ;; timeout-add

(define app (make <GtkApplication> #:application-id "org.gtk.timer"))
(define buf #f)

                                     ;; <CairoContext>
(define (on-expose-event drawingArea cr)
  (define cairoContext (context->cairo cr))

  (cairo-move-to       cairoContext 30 30)
  (cairo-set-font-size cairoContext 15)
  (cairo-show-text     cairoContext buf)

  (cairo-destroy       cairoContext)

  #f)

(define (time-handler win)
  (if (not win)
      #f
    (begin
      (set! buf (date->string (current-date) "~I:~M:~S"))

      (queue-draw win)

      #t)))

(define (timer app)
  (let ([window (make <GtkWindow>      #:application     app
                                       #:default-width   300
                                       #:default-height  200
                                       #:window-position (symbol->window-position
                                                           'center)
                                       #:title           "Timer")]
        [dArea  (make <GtkDrawingArea>)])
    (add window dArea)

    (connect dArea draw on-expose-event)

    (timeout-add PRIORITY_DEFAULT 1000 (lambda (iDunno) (time-handler window)))

    (show-all window)

    (time-handler window)))

(connect app activate timer)
(run app (command-line))
