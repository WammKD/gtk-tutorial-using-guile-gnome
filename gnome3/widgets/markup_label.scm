(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Label")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.markupLabel"))

(define (markup app)
  (let ([window (make <GtkWindow> #:application     app
                                  #:default-width   300
                                  #:default-height  100
                                  #:window-position (symbol->window-position
                                                      'center)
                                  #:title           "Markup label")]
        [label  (make <GtkLabel>  #:label "")])
    (set-markup label "<b>ZetCode</b>, knowledge only matters")

    (add window label)

    (show label)

    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate markup)
(run app (command-line))
