(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Alignment")
(load-by-name "Gtk" "Button")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.button"))

(define (button app)
  (let ([window (make <GtkWindow>    #:application     app
                                     #:default-width   230
                                     #:default-height  150
                                     #:border-width    15
                                     #:window-position (symbol->window-position
                                                         'center)
                                     #:title           "GtkButton")]
        [halign (make <GtkAlignment> #:xalign 0 #:yalign 0
                                     #:xscale 0 #:yscale 0)]
        [btn    (make <GtkButton>    #:label "Quit")])
    (add window halign)

    (set-size-request btn 70 30)

    (add halign btn)

    (connect btn    clicked (lambda (b) (quit)))
    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate button)
(run app (command-line))
