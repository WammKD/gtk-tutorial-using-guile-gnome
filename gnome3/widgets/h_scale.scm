(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Alignment")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "HBox")
;; (load-by-name "Gtk" "HScale")
(load-by-name "Gtk" "Label")
(load-by-name "Gtk" "Orientation")
(load-by-name "Gtk" "Range") ;; value-changed
(load-by-name "Gtk" "Scale")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.scale"))

;; signals aren't, any longer, handled by symbols (like in Guile-Gnome) but by
;; functions so we can't name this the same thing as the signal without overriding
(define (value-changed-helper range lbl)
  (set-text lbl (format #f "~,6f" (get-value range))))

(define (scale app)
  (let ([window (make <GtkWindow>    #:application     app
                                     #:default-width   300
                                     #:default-height  250
                                     #:border-width    10
                                     #:window-position (symbol->window-position
                                                         'center)
                                     #:title           "GtkHFrame")]
        [halign (make <GtkAlignment> #:xalign 0 #:yalign 0
                                     #:xscale 0 #:yscale 0)]
        [hbox   (make <GtkHBox>      #:homogeneous #f #:spacing 20)]
        [hscale ;; (hscale:new-with-range 0 100 1)  ;;; depreciated
                (scale:new-with-range (symbol->orientation 'horizontal) 0 100 1)]
        [label  (make <GtkLabel>     #:label "…" #:xalign 0.0 #:yalign 1)])
    (set-draw-value   hscale #f)
    (set-size-request hscale 150 -1)

    (pack-start hbox hscale #f #f 0)
    (pack-start hbox label  #f #f 0)

    (add halign hbox)
    (add window halign)

    (connect window destroy       (lambda (w) (quit)))
    (connect hscale value-changed (lambda (h) (value-changed-helper h label)))

    (show-all window)))

(connect app activate scale)
(run app (command-line))
