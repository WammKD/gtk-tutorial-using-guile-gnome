(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "Window") ;; set-title
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Alignment")
(load-by-name "Gtk" "Button")
(load-by-name "Gtk" "CheckButton")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "ToggleButton") ;; active
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.checkButton"))

(define (toggle-title checkButton win)
  (if (active checkButton)
      (set-title win "GtkCheckButton")
    (set-title win "")))

(define (check app)
  (let ([window (make <GtkWindow>      #:application     app
                                       #:default-width   230
                                       #:default-height  150
                                       #:border-width    15
                                       #:window-position (symbol->window-position
                                                           'center)
                                       #:title           "GtkCheckButton")]
        [halign (make <GtkAlignment>   #:xalign 0 #:yalign 0
                                       #:xscale 0 #:yscale 0)]
        [cb     (make <GtkCheckButton> #:label     "Show title"
                                       #:active    #t
                           ;; replaces GTK_WIDGET_UNSET_FLAGS(cb, GTK_CAN_FOCUS)
                                       #:can-focus #f)])
    (add window halign)
    (add halign cb)

    (connect window destroy (lambda (w) (quit)))
    (connect cb     clicked (lambda (c) (toggle-title c window)))

    (show-all window)))

(connect app activate check)
(run app (command-line))
