(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Label")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.label"))

(define (label app)
  (let ([window (make <GtkWindow> #:application     app
                                  #:border-width    15
                                  #:window-position (symbol->window-position
                                                      'center)
                                  #:title           "No Sleep")]
        [label  (make <GtkLabel>  #:label   (string-join
                                              '("I've always been too lame"
                                                "To see what's before me"
                                                "And I know nothing sweeter than"
                                                "Champaign from last New Years"
                                                "Sweet music in my ears"
                                                "And a night full of no fears"
                                                ""
                                                "But if I had one with fulfilled tonight"
                                                "I'd ask for the sun to never rise"
                                                "If God passed a mic to me to speak"
                                                "I'd say \"Stay in bed, world,"
                                                "Sleep in peace")
                                              "\n")
                                  #:justify 'center)])
    (add window label)

    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate label)
(run app (command-line))
