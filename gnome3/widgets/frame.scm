(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Frame")
(load-by-name "Gtk" "ShadowType")
(load-by-name "Gtk" "Table")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.frame"))

(define (frame app)
  (let ([window (make <GtkWindow> #:application     app
                                  #:default-width   250
                                  #:default-height  250
                                  #:border-width    10
                                  #:window-position (symbol->window-position
                                                      'center)
                                  #:title           "GtkFrame")]
        [table  (make <GtkTable>  #:n-rows      2  #:row-spacing    10
                                  #:n-columns   2  #:column-spacing 10
                                  #:homogeneous #t)]
        [frame1 (make <GtkFrame>  #:label       "Shadow In"
                                  #:shadow-type (symbol->shadow-type 'in))]
        [frame2 (make <GtkFrame>  #:label       "Shadow Out"
                                  #:shadow-type (symbol->shadow-type 'out))]
        [frame3 (make <GtkFrame>  #:label       "Shadow Etched In"
                                  #:shadow-type (symbol->shadow-type 'etched-in))]
        [frame4 (make <GtkFrame>  #:label       "Shadow Etched Out"
                                  #:shadow-type (symbol->shadow-type 'etched-out))])
    (add window table)

    (attach-defaults table frame1 0 1 0 1)
    (attach-defaults table frame2 0 1 1 2)
    (attach-defaults table frame3 1 2 0 1)
    (attach-defaults table frame4 1 2 1 2)

    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate frame)
(run app (command-line))
