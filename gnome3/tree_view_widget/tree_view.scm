(use-modules (gi) (gi repository) (gi types) (ice-9 receive))

(require "Gio"     "2.0")
(require "GObject" "2.0")
(require "Gtk"     "3.0")

(load-by-name "GObject" "Value")
(load-by-name "Gio"     "Application") ;; activate, run
(load-by-name "Gtk"     "Application")
(load-by-name "Gtk"     "ApplicationWindow")
(load-by-name "Gtk"     "WindowPosition")
(load-by-name "Gtk"     "Box") ;; pack-start
(load-by-name "Gtk"     "CellRendererText")
(load-by-name "Gtk"     "Container") ;; add
(load-by-name "Gtk"     "Statusbar")
(load-by-name "Gtk"     "TreeIter")
(load-by-name "Gtk"     "TreeModel") ;; get-value!
(load-by-name "Gtk"     "TreeSelection")
(load-by-name "Gtk"     "TreeStore")
(load-by-name "Gtk"     "TreeView")
(load-by-name "Gtk"     "TreeViewColumn")
(load-by-name "Gtk"     "VBox")
(load-by-name "Gtk"     "Widget") ;; show-all

;; While I would've liked to demonstrate how to make <GEnum>s – like
;; we did with Gnome2 –, I can't find a means to do it
;; Since it's not strictly necessary (we just converted them back to
;; values, again, with `genum->value`), we'll just do this
(define COLUMN   0)
(define NUM_COLS 1)



(define app (make <GtkApplication> #:application-id "org.gtk.treeView"))

(define (on-changed treeSelection sttsBar)
  (receive (_ model iter)
      (get-selected! treeSelection (make <GtkTreeIter>))
    (when (and model iter)
      (let ([value (get-value! model  iter
                               COLUMN (init (make <GValue>) G_TYPE_STRING))])
        (push sttsBar (get-context-id sttsBar (get-string value)) (get-string value))))))

(define (create-and-fill-model)
                 ;; We're doing it this way, rather than with `make`,
                 ;; since the doc.s don't list any properties for
                 ;; <GtkTreeStore>s and I don't want to have to set
                 ;; the types of the columns on a whole other line
  (define treestore (tree-store:new (vector G_TYPE_STRING)))

  (let ([toplevel        (append! treestore (make <GtkTreeIter>))])
    (set-value treestore toplevel
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "Scripting Languages")

                           value))

    (set-value treestore (append! treestore (make <GtkTreeIter>) toplevel)
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "Python")

                           value))

    (set-value treestore (append! treestore (make <GtkTreeIter>) toplevel)
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "Perl")

                           value))

    (set-value treestore (append! treestore (make <GtkTreeIter>) toplevel)
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "PHP")

                           value)))

  (let ([toplevel        (append! treestore (make <GtkTreeIter>))])
    (set-value treestore toplevel
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "Compiled Languages")

                           value))

    (set-value treestore (append! treestore (make <GtkTreeIter>) toplevel)
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "C")

                           value))

    (set-value treestore (append! treestore (make <GtkTreeIter>) toplevel)
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "C++")

                           value))

    (set-value treestore (append! treestore (make <GtkTreeIter>) toplevel)
               COLUMN    (let ([value (init (make <GValue>) G_TYPE_STRING)])
                           (set-string value "Java")

                           value)))

  treestore)

(define (create-view-and-model)
  (define view     (make <GtkTreeView>))
  (define col      (make <GtkTreeViewColumn> #:title "Programming Lanuages"))
  (define renderer (make <GtkCellRendererText>))
  (define model    (create-and-fill-model))

  (append-column view col)

  (pack-start    col renderer #t)
  (add-attribute col renderer "text" COLUMN)

  (set-model view model)

  view)

(define (tree-view app)
  (let* ([window    (make <GtkWindow>    #:application     app
                                         #:default-width   350
                                         #:default-height  300
                                         #:title           "Tree view"
                                         #:window-position (symbol->window-position
                                                             'center))]
         [view      (create-view-and-model)]
         [selection (get-selection view)]
         [vbox      (make <GtkVBox>      #:homogeneous #f #:spacing 0)]
         [statusbar (make <GtkStatusbar>)])
    (add window vbox)

    (pack-start vbox view      #t #t 1)
    (pack-start vbox statusbar #f #t 1)

    (connect selection changed (lambda (s) (on-changed s statusbar)))
    (connect window    destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate tree-view)
(run app (command-line))
