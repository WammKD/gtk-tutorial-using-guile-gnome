(use-modules (gi) (gi repository) (ice-9 receive))

(require "Gio"     "2.0")
(require "GObject" "2.0")
(require "Gtk"     "3.0")

(load-by-name "GObject" "Value")
(load-by-name "Gio"     "Application") ;; activate, run
(load-by-name "Gtk"     "Application")
(load-by-name "Gtk"     "ApplicationWindow")
(load-by-name "Gtk"     "WindowPosition")
(load-by-name "Gtk"     "Box") ;; pack-start
(load-by-name "Gtk"     "Button")
(load-by-name "Gtk"     "CellRendererText")
(load-by-name "Gtk"     "Container") ;; add
(load-by-name "Gtk"     "Entry")
(load-by-name "Gtk"     "HBox")
(load-by-name "Gtk"     "ListStore")
(load-by-name "Gtk"     "PolicyType")
(load-by-name "Gtk"     "ScrolledWindow")
(load-by-name "Gtk"     "ShadowType")
(load-by-name "Gtk"     "TreeIter")
(load-by-name "Gtk"     "TreeModel") ;; get-iter-first!
(load-by-name "Gtk"     "TreeSelection")
(load-by-name "Gtk"     "TreeView")
(load-by-name "Gtk"     "TreeViewColumn")
(load-by-name "Gtk"     "VBox")
(load-by-name "Gtk"     "Widget") ;; show-all

;; While I would've liked to demonstrate how to make <GEnum>s – like
;; we did with Gnome2 –, I can't find a means to do it
;; Since it's not strictly necessary (we just converted them back to
;; values, again, with `genum->value`), we'll just do this
(define LIST_ITEM 0)
(define N_COLUMNS 1)



(define app (make <GtkApplication> #:application-id "org.gtk.dynamicListView"))

(define (append-item entryObj treeView)
             ;; I'm not entirely certain if this actually casts
             ;; the <GtkTreeModel> to a <GtkListStore>…
  (define store    (make <GtkListStore> (get-model treeView)))
  (define entryStr (init (make <GValue>) G_TYPE_STRING))

  (set-string entryStr (get-text entryObj))

  ;; I can't see to get this to work; the documentation says
  ;; `(list-store:set self iter columns values)` but it keeps
  ;; throwing an error of
  ;; "No applicable method for #<<generic> set (1)> in call
  ;; (set #<<GtkTreeModel> 55e418afb400>
  ;;      #<<GtkTreeIter>  55e418afb170>
  ;;      0
  ;;      "")"
  ;; Since it says "columns" and "values", I tried wrapping the
  ;; last two arguments in lists and, then, vectors, each, but it
  ;; had no effect. So I dunno.
  ;;
  ;; EDIT: I've since altered `store`, above, to try casting to
  ;;       a <GtkListStore> (since that's what `list-store:set`,
  ;;       naturally, calls for, rather than a <GtkTreeModel>…).
  ;;
  ;;       I don't know if this actually works but, with attempting
  ;;       to feed a <GValue> for the last parameter (and not sure
  ;;       if I'm doing that right, either), we get back an warning
  ;;       of "gtk_list_store_set_value: assertion
  ;;       'iter_is_valid (iter, list_store)' failed".
  ;;
  ;;       Which…if I was going to guess at anything being wrong
  ;;       with this section, it would not have been the iter.
  ;;
  ;;       Maybe it's because the cast to <GtkListStore> isn't
  ;;       working; I dunno.
  ;;
  ;;       The program doesn't crash when you press "Add" – now –,
  ;;       though; so that's, kind of, an improvement.
  (set-value store     (append! (make <GtkListStore> store) (make <GtkTreeIter>))
             LIST_ITEM entryStr)
  (set-text entryObj ""))

(define (remove-item treeSelection treeV)
  (define store (get-model treeV))

  (when (get-iter-first! store (make <GtkTreeIter>))
    (receive (model iter)
        (get-selected! treeSelection (make <GtkTreeIter>))
      (when (and model iter)
        (remove store iter)))))

(define (remove-all tv)
  (define store (get-model tv))

  (when (get-iter-first! store (make <GtkTreeIter>))
    (clear store)))

(define (init-list tView)
             ;; We're doing it this way, rather than with `make`,
             ;; since the doc.s don't list any properties for
             ;; <GtkListStore>s and I don't want to have to set
             ;; the types of the columns on a whole other line
  (define store    (list-store:new (vector G_TYPE_STRING)))
  (define renderer (make <GtkCellRendererText>))
  (define column   (make <GtkTreeViewColumn> #:title "List Items"))

  (pack-start    column renderer #f)
  (add-attribute column renderer "text" LIST_ITEM)

  (append-column tView column)
  (set-model     tView store))

(define (dynamic app)
  (let ([window       (make <GtkWindow>         #:application     app
                                                #:border-width     10
                                                #:default-width   370
                                                #:default-height  270
                                                #:title           "List view"
                                                #:window-position (symbol->window-position
                                                                    'center))]
        [sw           (make <GtkScrolledWindow> #:shadow-type       (symbol->shadow-type
                                                                      'etched-in)
                                                #:hscrollbar-policy (symbol->policy-type
                                                                      'automatic)
                                                #:vscrollbar-policy (symbol->policy-type
                                                                      'automatic))]
        [   removeBtn (make <GtkButton>         #:label "Remove")]
        [      addBtn (make <GtkButton>         #:label "Add")]
        [removeAllBtn (make <GtkButton>         #:label "Remove All")]
        [entry        (make <GtkEntry>)]
        [vbox         (make <GtkVBox>           #:homogeneous #f #:spacing 0)]
        [hbox         (make <GtkHBox>           #:homogeneous #f #:spacing 0)]
        [lst          (make <GtkTreeView>       #:headers-visible #f)])
    (add sw lst)

    (pack-start vbox sw #t #t 5)

    (set-size-request entry 120 -1)

    (pack-start hbox       addBtn #f #t 3)
    (pack-start hbox entry        #f #t 3)
    (pack-start hbox    removeBtn #f #t 3)
    (pack-start hbox removeAllBtn #f #t 3)

    (pack-start vbox hbox         #f #t 3)

    (add window vbox)

    (init-list lst)

    (connect       addBtn clicked (lambda (a)  (append-item entry               lst)))
    (connect    removeBtn clicked (lambda (r)  (remove-item (get-selection lst) lst)))
    (connect removeAllBtn clicked (lambda (ra) (remove-all  lst)))
    (connect window       destroy (lambda (w)  (quit)))

    (show-all window)))

(connect app activate dynamic)
(run app (command-line))
