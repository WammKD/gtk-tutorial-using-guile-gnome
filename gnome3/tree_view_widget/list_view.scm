(use-modules (gi) (gi repository) (gi types) (ice-9 receive))

(require "Gio"     "2.0")
(require "GObject" "2.0")
(require "Gtk"     "3.0")

(load-by-name "GObject" "Value")
(load-by-name "Gio"     "Application") ;; activate, run
(load-by-name "Gtk"     "Application")
(load-by-name "Gtk"     "ApplicationWindow")
(load-by-name "Gtk"     "WindowPosition")
(load-by-name "Gtk"     "Box") ;; pack-start
(load-by-name "Gtk"     "CellRendererText")
(load-by-name "Gtk"     "Container") ;; add
(load-by-name "Gtk"     "Label")
(load-by-name "Gtk"     "ListStore")
(load-by-name "Gtk"     "TreeIter")
(load-by-name "Gtk"     "TreeModel") ;; get-value!
(load-by-name "Gtk"     "TreeSelection") ;; changed, apparently
(load-by-name "Gtk"     "TreeView")
(load-by-name "Gtk"     "TreeViewColumn")
(load-by-name "Gtk"     "VBox")
(load-by-name "Gtk"     "Widget") ;; show-all

;; While I would've liked to demonstrate how to make <GEnum>s – like
;; we did with Gnome2 –, I can't find a means to do it
;; Since it's not strictly necessary (we just converted them back to
;; values, again, with `genum->value`), we'll just do this
(define LIST_ITEM 0)
(define N_COLUMNS 1)



(define app (make <GtkApplication> #:application-id "org.gtk.listView"))

(define (init-list tView)
             ;; We're doing it this way, rather than with `make`,
             ;; since the doc.s don't list any properties for
             ;; <GtkListStore>s and I don't want to have to set
             ;; the types of the columns on a whole other line
  (define store    (list-store:new (vector G_TYPE_STRING)))
  (define renderer (make <GtkCellRendererText>))
  (define column   (make <GtkTreeViewColumn> #:title "List Items"))

  (pack-start    column renderer #f)
  (add-attribute column renderer "text" LIST_ITEM)

  (append-column tView column)
  (set-model     tView store))

(define (add-to-list treeView str)
  (define store (get-model treeView))

  (set-value store     (append! store (make <GtkTreeIter>))
             LIST_ITEM (let ([value (init (make <GValue>) G_TYPE_STRING)])
                         (set-string value str)

                         value)))

(define (on-changed treeSelection lbl)
  (receive (_ model iter)
      (get-selected! treeSelection (make <GtkTreeIter>))
    (when (and model iter)
      (set-text
        lbl
        (get-string (get-value! model     iter
                                LIST_ITEM (init (make <GValue>) G_TYPE_STRING)))))))

(define (list-view app)
  (let ([window (make <GtkWindow>   #:application     app
                                    #:default-width   270
                                    #:default-height  250
                                    #:border-width    10
                                    #:title           "List view"
                                    #:window-position (symbol->window-position
                                                        'center))]
        [lst    (make <GtkTreeView> #:headers-visible #f)]
        [vbox   (make <GtkVBox>     #:homogeneous #f #:spacing 0)]
        [label  (make <GtkLabel>    #:label "")])
    (pack-start vbox lst   #t #t 5)
    (pack-start vbox label #f #f 5)

    (add window vbox)

    (init-list   lst)
    (add-to-list lst "Aliens")
    (add-to-list lst "Leon")
    (add-to-list lst "The Verdict")
    (add-to-list lst "North Face")
    (add-to-list lst "Der Untergang")

    (connect (get-selection lst) changed (lambda (s) (on-changed s label)))
    (connect window              destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate list-view)
(run app (command-line))
