(use-modules (gi) (gi repository) (gi types))

(require "Gio" "2.0")
(require "Gdk" "3.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gdk" "Event") ;; get-event-type
(load-by-name "Gdk" "EventType")
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "Window")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "EventBox")
(load-by-name "Gtk" "Menu")
(load-by-name "Gtk" "MenuItem")
(load-by-name "Gtk" "MenuShell")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.popup"))

(define (show-popup menu event)
  (define RIGHT_CLICK 3)

  (if (equal? (get-event-type event) (symbol->event-type 'button-press))
      (begin
        ;; get-button just returns #t, for some reason…
        ;; (when (eq? (get-button event) RIGHT_CLICK)
          (popup-at-pointer menu event) ;; popup is depreciated
          ;; )

        #t)
    #f))

(define (popup a)
  (let ([window (make <GtkWindow>   #:title           "Popup menu"
                                    #:application     a
                                    #:default-width   300
                                    #:default-height  200
                                    #:window-position (symbol->enum
                                                        <GtkWindowPosition>
                                                        'center))]
        [ebox   (make <GtkEventBox>)]
        [pmenu  (make <GtkMenu>)]
        [hideMi (make <GtkMenuItem> #:label "Minimize")]
        [quitMi (make <GtkMenuItem> #:label "Quit")])
    (add window ebox)

    (show hideMi)
    (append pmenu hideMi)
    (connect hideMi activate (lambda (hMi)     (iconify window)))

    (show quitMi)
    (append pmenu quitMi)
    (connect quitMi activate (lambda (qMi)     (destroy window)))


    (connect ebox   event    (lambda (eBox ev) (show-popup pmenu ev)))

    (show-all window)))

(connect app activate popup)
(run app (command-line))
