(use-modules (gi) (gi repository) (gi types))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "CheckMenuItem")
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Menu")
(load-by-name "Gtk" "MenuBar")
(load-by-name "Gtk" "MenuItem")
(load-by-name "Gtk" "MenuShell")
(load-by-name "Gtk" "Statusbar")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.checkMenu"))

(define (toggle-statusbar togstat statusbar)
  (if (get-active? togstat) (show statusbar) (hide statusbar)))

(define (check-menuitem app)
  (let* ([window    (make <GtkWindow>        #:application     app
                                             #:default-width   300
                                             #:default-height  200
                                             #:window-position (symbol->enum
                                                                 <GtkWindowPosition>
                                                                 'center)
                                             #:title           "GtkCheckMenuItem")]
         [vbox      (make <GtkVBox>          #:homogeneous #f #:spacing 0)]
         [menubar   (make <GtkMenuBar>)]
         [viewMenu  (make <GtkMenu>)]
         [view      (make <GtkMenuItem>      #:label   "View"
                                             #:submenu viewMenu)]
         [togStat   (make <GtkCheckMenuItem> #:label  "View statusbar"
                                             #:active #t)]
         [statusbar (make <GtkStatusbar>)])
    (add window vbox)

    (append viewMenu togStat)
    (append menubar  view)

    (pack-start vbox menubar   #f #f 0)
    (pack-end   vbox statusbar #f #t 0)

    (connect togStat activate (lambda (ts) (toggle-statusbar ts statusbar)))
    ;; It may just be because there's nothing in the statusbar and my theme
    ;; hides it but it's not visible, which makes hiding it not
    ;; as…informative. Use this one to hide the menubar, instead.
    ;; You won't be able to make it visible, again (since the view menu item is
    ;; a member of the menubar and gets hidden, as well), but it'll, at least,
    ;; demonstrate that it works.
    ;; (connect togStat activate (lambda (ts) (toggle-statusbar ts menubar)))

    (show-all window)))

(connect app activate check-menuitem)
(run app (command-line))
