(use-modules (gi) (gi repository) (gi types))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Menu")
(load-by-name "Gtk" "MenuBar")
(load-by-name "Gtk" "MenuItem")
(load-by-name "Gtk" "MenuShell")
(load-by-name "Gtk" "SeparatorMenuItem")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.submenu"))

(define (submenu a)
  (let* ([window   (make <GtkWindow>    #:title           "Submenu"
                                        #:application     a
                                        #:default-width   300
                                        #:default-height  200
                                        #:window-position (symbol->enum
                                                            <GtkWindowPosition>
                                                            'center))]
         [vbox     (make <GtkVBox>      #:homogeneous #f #:spacing 0)]
         [menubar  (make <GtkMenuBar>)]
         [fileMenu (make <GtkMenu>)]
         [imprMenu (make <GtkMenu>)]
         [sep      (make <GtkSeparatorMenuItem>)]
         [fileMi   (make <GtkMenuItem> #:label   "File"
                                       #:submenu fileMenu)]
         [imprMi   (make <GtkMenuItem> #:label   "Import"
                                       #:submenu imprMenu)]
         [feedMi   (make <GtkMenuItem> #:label "Import news feed…")]
         [bookMi   (make <GtkMenuItem> #:label "Import bookmarks…")]
         [mailMi   (make <GtkMenuItem> #:label "Import mail…")]
         [quitMi   (make <GtkMenuItem> #:label "Quit")])
    (add window vbox)

    (append imprMenu feedMi)
    (append imprMenu bookMi)
    (append imprMenu mailMi)

    (append fileMenu imprMi)
    (append fileMenu sep)
    (append fileMenu quitMi)

    (append menubar  fileMi)

    (pack-start vbox menubar #f #f 0)

    (connect quitMi activate (lambda (q) (destroy window)))

    (show-all window)))

(connect app activate submenu)
(run app (command-line))
