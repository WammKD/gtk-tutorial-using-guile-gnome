(use-modules (gi) (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "Window")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Container") ;; add, set-border-width
(load-by-name "Gtk" "SeparatorToolItem")
(load-by-name "Gtk" "Toolbar")
(load-by-name "Gtk" "ToolbarStyle")
(load-by-name "Gtk" "ToolButton")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.toolbar"))

(define (toolbar app)
  (let ([window  (make <GtkWindow>            #:application     app
                                              #:default-width   250
                                              #:default-height  200
                                              #:title           "toolbar"
                                              #:window-position (symbol->window-position
                                                                  'center))]
        [vbox    (make <GtkVBox>              #:homogeneous     #f
                                              #:spacing         0)]
        [toolbar (make <GtkToolbar>           #:toolbar-style   (symbol->toolbar-style
                                                                  'icons))]
        [new     (make <GtkToolButton>        #:stock-id        "gtk-new")]
        [open    (make <GtkToolButton>        #:stock-id        "gtk-open")]
        [save    (make <GtkToolButton>        #:stock-id        "gtk-save")]
        [sep     (make <GtkSeparatorToolItem>)]
        [exit    (make <GtkToolButton>        #:stock-id        "gtk-quit")])
    (add window vbox)

    (set-border-width toolbar 2)

    (insert toolbar new  -1)
    (insert toolbar open -1)
    (insert toolbar save -1)
    (insert toolbar sep  -1)
    (insert toolbar exit -1)

    (pack-start vbox toolbar #f #f 5)

    (connect exit   clicked (lambda (w) (quit)))
    (connect window destroy (lambda (w) (quit)))

    (show-all window)))

(connect app activate toolbar)
(run app (command-line))
