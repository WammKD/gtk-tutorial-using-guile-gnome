(use-modules (gi) (gi repository) (gi types))
(use-typelibs ("Gdk" "3.0")) ;; 'Can't figure out where keyval-from-name is from

(require "Gio" "2.0")
;; (require "Gdk" "3.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
;; (load-by-name "Gdk" "ModifierType")
(load-by-name "Gtk" "AccelFlags")
(load-by-name "Gtk" "AccelGroup")
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "Window") ;; add-accel-group
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "ImageMenuItem")
(load-by-name "Gtk" "Menu")
(load-by-name "Gtk" "MenuBar")
(load-by-name "Gtk" "MenuItem")
(load-by-name "Gtk" "MenuShell")
(load-by-name "Gtk" "SeparatorMenuItem")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.mnemonics"))

(define (images-mnemonics-accelerators a)
  (let* ([window     (make <GtkWindow>    #:application     a
                                          #:default-width   300
                                          #:default-height  200
                                          #:window-position (symbol->enum
                                                              <GtkWindowPosition>
                                                              'center)
                                          #:title           "Images")]
         [vbox       (make <GtkVBox>      #:homogeneous #f #:spacing 0)]
         [menubar    (make <GtkMenuBar>)]
         [fileMenu   (make <GtkMenu>)]
         [fileMi     (menu-item:new-with-mnemonic "_File")]
         [ newMi     (image-menu-item:new-from-stock
                       "gtk-new"
                       (make <GtkAccelGroup>))]
         [openMi     (image-menu-item:new-from-stock
                       "gtk-open"
                       (make <GtkAccelGroup>))]
         [sep        (make <GtkSeparatorMenuItem>)]
         [accelGroup (make <GtkAccelGroup>)]
         [quitMi     (image-menu-item:new-from-stock "gtk-quit" accelGroup)])
    (add window vbox)

    (add-accel-group window accelGroup)

    (add-accelerator
      quitMi
      "activate"
      accelGroup
      (keyval-from-name "q")
      (symbol->enum <GdkModifierType> 'control-mask)
      (symbol->enum <GtkAccelFlags>   'visible))

    (set-submenu fileMi fileMenu)

    (append fileMenu  newMi)
    (append fileMenu openMi)
    (append fileMenu sep)
    (append fileMenu quitMi)
    (append menubar  fileMi)

    (pack-start vbox menubar #f #f 0)

    (connect window destroy  (lambda (w) (quit)))
    (connect quitMi activate (lambda (q) (destroy window)))

    (show-all window)))

(connect app activate images-mnemonics-accelerators)
(run app (command-line))
