(use-modules (gi) (gi repository) (gi types))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "Application") ;; activate, run
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "WindowPosition")
(load-by-name "Gtk" "Box") ;; pack-start
(load-by-name "Gtk" "Container") ;; add
(load-by-name "Gtk" "Menu")
(load-by-name "Gtk" "MenuBar")
(load-by-name "Gtk" "MenuItem")
(load-by-name "Gtk" "MenuShell")
(load-by-name "Gtk" "VBox")
(load-by-name "Gtk" "Widget") ;; show-all

(define app (make <GtkApplication> #:application-id "org.gtk.simpleMenu"))

(define (simple-menu a)
  (let* ([window   (make <GtkWindow>   #:title           "menu"
                                       #:application     a
                                       #:default-width   240
                                       #:default-height  200
                                       #:window-position (symbol->enum
                                                           <GtkWindowPosition>
                                                           'center))]
         [vbox     (make <GtkVBox>     #:homogeneous #f #:spacing 0)]
         [menubar  (make <GtkMenuBar>)]
         [filemenu (make <GtkMenu>)]
         [file     (make <GtkMenuItem> #:label   "File"
                                       #:submenu filemenu)]
         [quit     (make <GtkMenuItem> #:label "Quit")])
    (add window vbox)

    (append filemenu quit)
    (append menubar  file)

    (pack-start vbox menubar #f #f 3)

    (connect quit activate (lambda (q) (destroy window)))

    (show-all window)))

(connect app activate simple-menu)
(run app (command-line))
